class Store {
  constructor() {
    this._state = {
      fgType: '',
    };
  }
  install(Vue, options) {
    if (store) {
      Vue.prototype.$MyStore = store;
    }
  }
  get state() {
    return this._state;
  }
  resetStore() {
    this.state.fgType = '';
  }
}
const store = new Store();
export default store;