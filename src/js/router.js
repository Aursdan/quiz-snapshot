import Vue from 'vue';
import Router from 'vue-router';
import Quiz from 'js/components/Quiz.vue';
import Snapshot from 'js/components/Snapshot.vue';
import Store from './store';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      component: Quiz,
    },
    {
      path: '/snapshot',
      component: Snapshot,
      beforeEnter(to, from, next) {
        if (!Store.state.fgType) {
          next('/');
        } else {
          next();
        }
      }
    }
  ]
});
