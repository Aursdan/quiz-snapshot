import "babel-polyfill";
import Vue from 'vue';
import App from './App.vue';
import Store from './store';
import router from './router';

Vue.use(Store);

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router
});
