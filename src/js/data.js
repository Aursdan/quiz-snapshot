export const questions = [
  {
    desc: '陽光微煦的午後，和心儀的對象在咖啡館碰面，你會點：',
    answers: [
      { text: '熱咖啡', directTo: 2 },
      { text: '冰咖啡', directTo: 3 },
    ]
  },
  {
    desc: '服務人員詢問你想要的甜度，你會選：',
    answers: [
      { text: '砂糖', directTo: 4 },
      { text: '不加糖', directTo: 5 },
      { text: '果糖', directTo: 6 },
    ]
  },
  {
    desc: '店員問你想要那種容器裝咖啡，你會選：',
    answers: [
      { text: '馬克杯', directTo: 9 },
      { text: '外帶紙杯', directTo: 4 }
    ]
  },
  {
    desc: '聊到一半，對方忽然問你想不想吃點點心，你會選：',
    answers: [
      { text: '精緻蛋糕', directTo: 6 },
      { text: '都不吃', directTo: 8 },
      { text: '手工餅乾', directTo: 9 }
    ]
  },
  {
    desc: '看著心儀的對象，希望可以每天一起喝杯咖啡，關於咖啡，你最在意的是：',
    answers: [
      { text: '品牌', directTo: 6 },
      { text: '咖啡濃度', directTo: 10 },
      { text: '豆子新鮮度', directTo: 11 }
    ]
  },
  {
    desc: '隨著你們的話匣子打開，才知道對方居然對咖啡也頗有研究，還拿出了花式咖啡的密集，看到這本秘笈，你會選：',
    answers: [
      { text: '迫不及待想嘗試一下', directTo: 7 },
      { text: '無論如何絕對不自己動手', directTo: 11 },
      { text: '按兵不動，先看內容再想', directTo: 8 }
    ]
  },
  {
    desc: '看對方那麼喜歡咖啡，你在心裡盤算著下一次兩個人約見面的地方，你會選：',
    answers: [
      { text: '家裡', directTo: 10 },
      { text: '咖啡館', directTo: 12 },
      { text: '豪華餐館', directTo: 9 }
    ]
  },
  {
    desc: '欣賞的對象很喜歡咖啡，那麼自己喝咖啡的習慣是：',
    answers: [
      { text: '早晨', directTo: 10 },
      { text: '午後', directTo: 12 },
      { text: '晚餐', directTo: 11 }
    ]
  },
  {
    desc: '喝咖啡的習慣可以看出一個人的個性，你情不自禁的猜想對方的個性應該是：',
    answers: [
      { text: '大口喝完', directTo: 11 },
      { text: '在變冷前慢慢享受', directTo: 12 },
      { text: '變冷了也沒有關係，別有風味', directTo: 10 }
    ]
  },
  {
    desc: '你們聊的太開心，等發現時店內的音樂已經換了風格，你覺得會是：',
    answers: [
      { text: '古典樂', directTo: 'B' },
      { text: 'Jazz', directTo: 'A' },
      { text: '流行樂', directTo: 11 }
    ]
  },
  {
    desc: '如果有人邀請你的心儀對象拍電影，你覺得會是：',
    answers: [
      { text: '浪漫愛情', directTo: 'A' },
      { text: '炫技動作', directTo: 'C' },
      { text: '溫馨小品', directTo: 12 }
    ]
  },
  {
    desc: '咖啡店內有咖啡占卜的服務，但是需要額外付費，你有興趣試試看嗎？ ',
    answers: [
      { text: '有', directTo: 'D' },
      { text: '不了，謝謝', directTo: 'C' }
    ]
  }
];
export const types = {
  A: {
    name: '甜香摩卡 Moka',
    content: '摩卡中甜甜的巧克力就是你的化身，濃郁的香氣令人難以抗拒，只要身邊出現喜歡的人，你為愛改變的一切細節，都會被發現。愛情是你的罩門，一旦愛情來了，意味著改變也正要開始，如同在咖啡中嚐到的巧克力味，讓層次更豐富！'
  },
  B: {
    name: '俏皮卡布 Cappuccino',
    content: '咖啡和牛奶一半一半的比例，將咖啡的苦味轉換成牛奶的奶香，俏皮如你，像玩遊戲的孩子一般富有闖關的勇氣，卻又有大人的深思熟慮，各一半的風味組成了如此特別的你。'
  },
  C: {
    name: '芳醇濃縮 Expresso',
    content: '小小一杯濃縮，用本身的原味拉住你所有的注意力，在人群中的你就是如此不容忽視，火熱的溫度燃燒著所有熱情，會讓和你相處的人覺得新鮮有趣，如同濃縮咖啡在口中刺激著味蕾似的深刻，品嚐一次就永難忘懷。'
  },
  D: {
    name: '皇家拿鐵 Latte',
    content: '懂得自己喜好的你，牛奶與咖啡完美比例的拿鐵是你的代表。總是恰如其分的表現出得體的個人風格。對生活的理性和感性兼具，讓你在喘不過氣的日常生活中，找到屬於自己的濃郁平衡，如同皇家風範一般，優雅滑順。'
  }
}
