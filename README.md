 心理測驗 + 拍照上傳活動 App
===
 
* 發送資料的部分並未接上api，只有使用提示
* 只能在支援[getUserMedia](https://caniuse.com/#feat=stream)函數的瀏覽器中使用
* data 用來放置設定測驗相關的資料
    * questions 測驗的問題內容及答案對應的後續題目
    * types 最後的結果和前景圖片路徑
* store 儲存app內需要跨元件的狀態、資料
* components
    * InfoForm 聯絡資訊表單
    * Quiz 心理測驗的頁面
    * Snapshot 使用Seriously.js結合測驗答案的圖片和攝影機進行快拍的元件
