var path = require('path')
var MiniCssExtractPlugin = require('mini-css-extract-plugin')
var HtmlWebpackPlugin = require("html-webpack-plugin")
var webpack = require('webpack')
var VueLoaderPlugin = require('vue-loader/lib/plugin')
var WriteFilePlugin = require( 'write-file-webpack-plugin')
module.exports = {
    mode: 'development',
    entry: {
        main: path.resolve(__dirname, './src/js/index.js'),
    },
    output: {
        filename: 'assets/js/[name].js',
        path: path.resolve(__dirname, 'build')
    },
    resolve: {
        alias: {
            src: path.resolve(__dirname, 'src'),
            lib: path.resolve(__dirname, 'lib'),
            vue: 'vue/dist/vue.js'
        },
        modules: ['.', 'node_modules'],
    },
    externals: {
  		jquery: "jQuery",
  	},
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
              test: /\.(png|woff|woff2|eot|ttf|svg)$/,
              loader: 'url-loader?limit=100000'
            },
            {
              test: /\.(css|scss)$/,
              use: [
                process.env.NODE_ENV !== 'production'
                ? 'vue-style-loader' : MiniCssExtractPlugin.loader,
                'css-loader?url=false',
                'sass-loader',
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: [
                      require('autoprefixer')({ browsers: ['last 6 versions'] }),
                    ]
                  }
                }
              ]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        // webpack-dev-server 更新時同時寫入檔案的插件
        new WriteFilePlugin({
          // 清除寫入的hot reload資訊檔
          test: /^(?!.*(hot)).*/,
        }),
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: "assets/css/[name].css"
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/ejs/index.ejs',
            hash: true,
            chunks: ['main']
        }),
        // new webpack.DefinePlugin({
        //     'process.env': {
        //         NODE_ENV: '"production"'
        //     }
        // }),
    ],
    devServer: {
      contentBase: './build',
      // hot: true,
      // for serveo
      disableHostCheck: true
    },
}
